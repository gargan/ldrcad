#!/usr/bin/python3
"""
insert something here
"""
import sys
import wx
import wx.glcanvas
import numpy as np
import math

from OpenGL.GL import *

from typing import Any


class LDrawModel:
    def __init__(self, gl_canvas):
        self.gl_canvas: LDRCadGLCanvas = gl_canvas
        self.gl_canvas.model = self

        # for now the data is hardcoded
        self.vertex_buffer_data = np.array([
            -1.0, -1.0, -1.0,
            -1.0, -1.0, 1.0,
            -1.0, 1.0, 1.0,
            1.0, 1.0, -1.0,
            -1.0, -1.0, -1.0,
            -1.0, 1.0, -1.0,
            1.0, -1.0, 1.0,
            -1.0, -1.0, -1.0,
            1.0, -1.0, -1.0,
            1.0, 1.0, -1.0,
            1.0, -1.0, -1.0,
            -1.0, -1.0, -1.0,
            -1.0, -1.0, -1.0,
            -1.0, 1.0, 1.0,
            -1.0, 1.0, -1.0,
            1.0, -1.0, 1.0,
            -1.0, -1.0, 1.0,
            -1.0, -1.0, -1.0,
            -1.0, 1.0, 1.0,
            -1.0, -1.0, 1.0,
            1.0, -1.0, 1.0,
            1.0, 1.0, 1.0,
            1.0, -1.0, -1.0,
            1.0, 1.0, -1.0,
            1.0, -1.0, -1.0,
            1.0, 1.0, 1.0,
            1.0, -1.0, 1.0,
            1.0, 1.0, 1.0,
            1.0, 1.0, -1.0,
            -1.0, 1.0, -1.0,
            1.0, 1.0, 1.0,
            -1.0, 1.0, -1.0,
            -1.0, 1.0, 1.0,
            1.0, 1.0, 1.0,
            -1.0, 1.0, 1.0,
            1.0, -1.0, 1.0
        ], dtype=np.float32)

        self.color_buffer_data = np.array([
            0.583, 0.771, 0.014,
            0.609, 0.115, 0.436,
            0.327, 0.483, 0.844,
            0.822, 0.569, 0.201,
            0.435, 0.602, 0.223,
            0.310, 0.747, 0.185,
            0.597, 0.770, 0.761,
            0.559, 0.436, 0.730,
            0.359, 0.583, 0.152,
            0.483, 0.596, 0.789,
            0.559, 0.861, 0.639,
            0.195, 0.548, 0.859,
            0.014, 0.184, 0.576,
            0.771, 0.328, 0.970,
            0.406, 0.615, 0.116,
            0.676, 0.977, 0.133,
            0.971, 0.572, 0.833,
            0.140, 0.616, 0.489,
            0.997, 0.513, 0.064,
            0.945, 0.719, 0.592,
            0.543, 0.021, 0.978,
            0.279, 0.317, 0.505,
            0.167, 0.620, 0.077,
            0.347, 0.857, 0.137,
            0.055, 0.953, 0.042,
            0.714, 0.505, 0.345,
            0.783, 0.290, 0.734,
            0.722, 0.645, 0.174,
            0.302, 0.455, 0.848,
            0.225, 0.587, 0.040,
            0.517, 0.713, 0.338,
            0.053, 0.959, 0.120,
            0.393, 0.621, 0.362,
            0.673, 0.211, 0.457,
            0.820, 0.883, 0.371,
            0.982, 0.099, 0.879
        ], dtype=np.float32)

        self.gl_canvas.update_vbo()


class LDRCadGLCanvas(wx.glcanvas.GLCanvas):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self.is_gl_init = False
        self.context = wx.glcanvas.GLContext(self)
        self.size = None
        self.model: LDrawModel = None

        self.mouse_pos = None

        self.vbo = None
        self.cbo = None
        self.shader_program = None

        self.camera_fov = 45.0

        self.identity_mat4 = np.matrix([[1, 0, 0, 0],
                                        [0, 1, 0, 0],
                                        [0, 0, 1, 0],
                                        [0, 0, 0, 1]])

        self.projection_mat = self.identity_mat4
        self.view_mat = self.look_at(np.array([4, 3, 3]), np.array([0, 0, 0]), np.array([0, 1, 0]))
        self.model_mat = self.identity_mat4
        self.update_mvp()

        self.Bind(wx.EVT_SIZE, self.on_size)
        self.Bind(wx.EVT_PAINT, self.on_paint)

        self.Bind(wx.EVT_RIGHT_DOWN, self.on_mouse_event)
        self.Bind(wx.EVT_RIGHT_UP, self.on_mouse_event)
        self.Bind(wx.EVT_MOTION, self.on_mouse_event)

    def on_mouse_event(self, event: wx.MouseEvent) -> None:
        if event.RightDown():
            self.mouse_pos = event.Get
            print("Left Down")
        elif event.RightUp():
            print("Left Up")
        elif event.Dragging():
            print ("Dragging")

    def on_size(self, event: wx.Event) -> None:
        wx.CallAfter(self.set_viewport)
        event.Skip()

    def set_viewport(self) -> None:
        self.size = self.GetClientSize()

        # update projection matrix
        self.projection_mat = self.perspective(self.camera_fov, self.size.width / self.size.height, 0.1, 100.0)
        self.update_mvp()

        self.SetCurrent(self.context)
        glViewport(0, 0, self.size.width, self.size.height)

    def on_paint(self, event: wx.Event) -> None:
        self.SetCurrent(self.context)

        if not self.is_gl_init:
            self.init_gl()
            self.is_gl_init = True

        # clear color and depth buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        glUniformMatrix4fv(self.matrix_id, 1, GL_TRUE, np.ascontiguousarray(self.mvp_mat, dtype=np.float32))

        # Draw data
        if self.model is not None:
            glDrawArrays(GL_TRIANGLES, 0, self.model.vertex_buffer_data.size//3)

        self.SwapBuffers()

    def init_gl(self) -> None:
        glEnable(GL_DEPTH_TEST)
        glDepthFunc(GL_LESS)

        # Set background color
        glClearColor(0, 0, 0.4, 0)

        # Create the VAO
        vao = glGenVertexArrays(1)
        glBindVertexArray(vao)

        # Create and compile basic shaders
        VERTEX_SHADER = """#version 330 core
        layout(location = 0) in vec3 aPos;
        layout(location = 1) in vec3 aCol;
        
        out vec3 fragmentColor;

        uniform mat4 MVP;
        void main(){
          gl_Position = MVP * vec4(aPos.x, aPos.y, aPos.z, 1.0);
          fragmentColor = aCol;
        }"""

        FRAGMENT_SHADER = """#version 330 core
        in vec3 fragmentColor;
        out vec4 color;
        void main(){
          color = vec4(fragmentColor.r, fragmentColor.g, fragmentColor.b, 1.0f);
          //color = vec4(1, 1, 1, 0);
        }"""

        vertexshaderid = glCreateShader(GL_VERTEX_SHADER)
        fragshaderid = glCreateShader(GL_FRAGMENT_SHADER)

        glShaderSource(vertexshaderid, VERTEX_SHADER)
        glCompileShader(vertexshaderid)

        glShaderSource(fragshaderid, FRAGMENT_SHADER)
        glCompileShader(fragshaderid)

        self.shader_program = glCreateProgram()
        glAttachShader(self.shader_program, vertexshaderid)
        glAttachShader(self.shader_program, fragshaderid)
        glLinkProgram(self.shader_program)
        glUseProgram(self.shader_program)

        glDetachShader(self.shader_program, vertexshaderid)
        glDetachShader(self.shader_program, fragshaderid)

        glDeleteShader(vertexshaderid)
        glDeleteShader(fragshaderid)

        self.matrix_id = glGetUniformLocation(self.shader_program, 'MVP')

        # Create VBO
        if self.model is not None:
            vertices = self.model.vertex_buffer_data
        else:
            vertices = np.array([0.0], dtype=np.float32)

        self.vbo = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.vbo)
        glBufferData(GL_ARRAY_BUFFER, vertices.nbytes, vertices, GL_STATIC_DRAW)

        # Create Color Buffer
        if self.model is not None:
            colors = self.model.color_buffer_data
        else:
            colors = np.array([0.0], dtype=np.float32)

        self.cbo = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.cbo)
        glBufferData(GL_ARRAY_BUFFER, colors.nbytes, colors, GL_STATIC_DRAW)

        # Enable vertex attributes and define
        glEnableVertexAttribArray(0)
        glBindBuffer(GL_ARRAY_BUFFER, self.vbo)
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, None)

        # Enable color attributes and define
        glEnableVertexAttribArray(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.cbo)
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, None)

    def update_vbo(self) -> None:
        # update the vbo with the current model mesh data, if gl is not initialized, defer this until then
        if self.is_gl_init and self.model is not None:
            glBindBuffer(GL_ARRAY_BUFFER, self.vbo)
            glBufferData(GL_ARRAY_BUFFER, self.model.vertex_buffer_data.nbytes, self.model.vertex_buffer_data, GL_STATIC_DRAW)

            glBindBuffer(GL_ARRAY_BUFFER, self.cbo)
            glBufferData(GL_ARRAY_BUFFER, self.model.color_buffer_data.nbytes, self.model.color_buffer_data, GL_STATIC_DRAW)

    def update_mvp(self) -> None:
        self.mvp_mat = np.matmul(self.projection_mat, np.matmul(self.view_mat, self.model_mat))

    def perspective(self, fovy, aspect, n, f):
        s = 1.0 / math.tan(math.radians(fovy) / 2.0)
        sx, sy = s / aspect, s
        zz = (f + n) / (n - f)
        zw = 2 * f * n / (n - f)
        return np.matrix([[sx, 0, 0, 0],
                          [0, sy, 0, 0],
                          [0, 0, zz, zw],
                          [0, 0, -1, 0]])

    def magnitude(self, v):
        return math.sqrt(np.sum(v ** 2))

    def normalize(self, v):
        m = self.magnitude(v)
        if m == 0:
            return v
        return v / m

    def translate(self, xyz):
        x, y, z = xyz
        return np.matrix([[1, 0, 0, x],
                          [0, 1, 0, y],
                          [0, 0, 1, z],
                          [0, 0, 0, 1]])

    def look_at(self, eye, target, up):
        F = target[:3] - eye[:3]
        f = self.normalize(F)
        U = self.normalize(up[:3])
        s = np.cross(f, U)
        u = np.cross(s, f)
        M = np.matrix(np.identity(4))
        M[:3, :3] = np.vstack([s, u, -f])
        T = self.translate(-eye)
        return M * T


class LDRCadFrame(wx.Frame):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

        # main opengl canvas
        c = LDRCadGLCanvas(self)

        # model passing canvas as parameter
        self.model = LDrawModel(c)

        # create the main sizer
        sizer = wx.BoxSizer()
        sizer.Add(c, proportion=1, flag=wx.EXPAND)

        self.SetSizer(sizer)
        self.SetAutoLayout(True)


class LDRCadApp(wx.App):
    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)

    def OnInit(self) -> bool:
        frame = LDRCadFrame(None, title='HelloWorld')
        self.SetTopWindow(frame)

        frame.Show()

        return True


if __name__ == '__main__':
    app = LDRCadApp()
    sys.exit(app.MainLoop())
